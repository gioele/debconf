#!/usr/bin/perl

=head1 NAME

Debconf::Element::Editor::Note - Just text to display to user.

=cut

package Debconf::Element::Editor::Note;
use warnings;
use strict;
use base qw(Debconf::Element::Editor::Text);

1
