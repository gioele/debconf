#!/usr/bin/perl

=head1 NAME

Debconf::Element::Noninteractive::Multiselect - dummy multiselect Element

=cut

package Debconf::Element::Noninteractive::Multiselect;
use warnings;
use strict;
use base qw(Debconf::Element::Noninteractive);

=head1 DESCRIPTION

This is a dummy multiselect element.

=cut

1
